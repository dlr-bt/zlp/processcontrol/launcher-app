# Process Control launcher app
This project does not contain code besides a `main` method which launches the Process Control main method.

The purpose is to provide a single project that glues together all relevant components for ProcessControl using Maven. At the moment, this includes the OPC/UA executor.

## Download

[Download JAR file](/../-/jobs/artifacts/main/browse/target?job=build)

## Java development and runntime environment
A JDK or JRE with at least version 17 is required. While most likely any JDK/JRE will work, Adoptium is recommended: [https://adoptium.net/](https://adoptium.net/)

## License
Process Control is licensed under the Apache 2.0 license.

Copyright 2022 Deutsches Zentrum für Luft- und Raumfahrt e.V. (German Aerospace Center)
